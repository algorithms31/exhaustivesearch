/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.sarocha.exhaustivesearch;

/**
 *
 * @author Sarocha
 */
public class ExhaustiveSearch {

    public static boolean isPartition(int[] num) {
        int sum = 0;
        for (int i = 0; i < num.length; i++) {
            sum += num[i];
        }
        if (sum % 2 != 0) {
            return false;
        }
        return findPartition(num, sum / 2, 0);
    }

    public static boolean findPartition(int[] num, int sum, int index) {
        if (sum == 0) {
            return true;
        }

        if (num.length == 0 || index >= num.length) {
            return false;
        }

        if (num[index] <= sum) {
            if (findPartition(num, sum - num[index], index + 1)) {
                return true;
            }
        }
        return findPartition(num, sum, index + 1);
    }

    public static void main(String[] args) {
        int[] num = {5, 24, 35, 12, 4, 72};
        System.out.println(isPartition(num));
    }
}
